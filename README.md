
## Founder of SMARTRAY STUDIO

SMARTRAY STUDIO is a studio running self-owned mobile app business monetizing with Google Ads. I am responsible for all phases of the app product life cycle - from design, develop, test, CI/CD, TestFlight to release, including coding for all font-end and back-end, handling Server maintenance, marketing and customer service. All apps run on my own cloud platform (LAMP, Memcahced, Redis, gRPC, Java) in Tokyo, Japan and Fremont, USA Data Centers.  

## Specialties

Objective-C, Swift, Android, Java, C/C++, PHP, gRPC, RESTful.  
Deep Learning, Tensorflow, Keras, Python.  
Cloud Computing, GCP, Firebase, AWS.  
Large-scale system design and development.  
GPU Computing, CUDA.  
AGILE, CI/CD.  


## Products

### English Radio - IELTS TOEFL

SNS app for English learners to improve English listening and speaking skills, especially useful for those who are preparing for English exams, i.e. IELTS, TOFEL. Written in Objective-C/Java. Released in Oct, 2013. 

![picture](App/EnglishRadio/er_2.png)

![picture](App/EnglishRadio/er_1.png) 

[Download from App Store](https://itunes.apple.com/app/id724108987)  

[Download from Google Play](https://play.google.com/store/apps/details?id=com.smartray.englishradio)  

[Visit Website](http://www.englishradio.com.au/)


### Spellout - English Dictation
Spellout is a simple, powerful app for English learners to improve their listening, vocabulary, reading, and pronunciation skills. It a modern app with many useful features in a simple, intuitive interface. Written in Objective-C. Released in Mar, 2018.

![picture](App/Spellout/sp_1.png)  

![picture](App/Spellout/sp_400.gif)

[Download from App Store](https://itunes.apple.com/app/id1239139220)  

[Visit Website](http://spellout.englishradio.com.au/)


### Japan Radio 
Listen to Japanese Radio. Meet new people and make friends. Written in Objective-C/Java. Released in Dec, 2014.

FEATURES: 
Popular radios from Japan. 
Making friends has never been simpler. 
Free texting, voice messages, sending photos and customised animated stickers. Get message alerts instantly with push notifications. 
Share your moments, Like and comment on photos with your friends. 
Talk to people in chatroom.

![picture](App/JapanRadio/jr_1.png)

[Download from App Store](https://itunes.apple.com/app/id786922884)

[Download from Google Play](https://play.google.com/store/apps/details?id=com.smartray.japanradio)  

[Visit Website](http://www.japanradio.com.au/)


### RssPod

RssPod is a modern, fully featured and FREE RSS Podcast player, with many useful features in a simple, intuitive interface. It help you listen to more podcasts in more places, try new shows, and completely cotrol your experience. Written in Swift. 

![picture](App/RssPod/rsspod_2.png)

[Download from App Store](https://itunes.apple.com/app/id1317643805)



### AEB - Learn English
Free app for English learners. Special and standard American English programs and SNS platform.  Written in Objective-C/Java. Released in Jan, 2014.

![picture](App/AEB/aeb_1.png)

[Download from App Store](https://itunes.apple.com/app/id788455068)  

[Download from Google Play](https://play.google.com/store/apps/details?id=com.smartray.voa)



### English Corner
Improve English listening and speaking skills. Meet new people and make friends all over the world. Written in Objective-C. Released in Apr, 2013.

![picture](App/EnglishCorner/ec_1.png)

[Download from App Store](https://itunes.apple.com/app/id639373594)


### Chinese Radio
Popular radio in Mandarin, Cantonese. It supports iPhone, iPod touch, and iPad. Written in Objective-C. Released in Feb, 2014.(Deprecated)

![picture](App/ChineseRadio/cr_1.png)

[Download from App Store](https://itunes.apple.com/app/id806447963)

### Barbarian Warrior vs Zombie Defense ACT TD - Hammer of Thor
In the fantasy world. You lead the army to protect the magic gem, defeat the enemies, earn gold coins, upgrade your hero, upgrade your skills, soldiers. Collect unique equipment to arm your army! Written in Objective-C. Released in Sept, 2013. (Deprecated)

![picture](App/BvZ/bvz_1.png)

[Download from App Store](https://itunes.apple.com/app/id678249911)



## Contact

Email: [YuanLiang7887@gmail.com](mailto:YuanLiang7887@gmail.com)

